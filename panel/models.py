from django.contrib.auth.models import User
from django.db import models

class ShoppingItem(models.Model):
    STATUSES = (
        ('requested', 'Requested'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected')
    )

    requestor = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_created=True)
    item = models.CharField(max_length=128)
    justification = models.TextField(blank=True)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    status = models.CharField(max_length=32, choices=STATUSES, default='requested')

    def __str__(self):
        return "%s (by %s)" % (self.item, self.requestor)