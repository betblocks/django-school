from django.conf.urls import patterns, url
from panel.views import HomeView, ShoppingItemListView

urlpatterns = patterns('panel',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^list$', ShoppingItemListView.as_view(), name='shopping_item_list'),
)