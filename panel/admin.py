from django.contrib import admin
from panel.models import ShoppingItem

class ShoppingItemAdmin(admin.ModelAdmin):
    pass

admin.site.register(ShoppingItem, ShoppingItemAdmin)