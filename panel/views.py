from django.views.generic import TemplateView, ListView
from panel.models import ShoppingItem


class HomeView(TemplateView):
    template_name = "panel/home.html"


class ShoppingItemListView(ListView):
    model = ShoppingItem
    template_name = "panel/shopping_items.html"